/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.departments;

import best.software.inc.people.Employee;
import best.software.inc.people.Manager;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author emman
 */
public abstract class Department {

    private static int departmentID;
    private Manager manager;
    private List<Employee> employees;

    public Department() {
        departmentID++;
        employees = new ArrayList<>();
    }

    public static int getDepartmentID() {
        return departmentID;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void addEmployee(Employee employee) {
        if (employee != null) {
            employees.add(employee);
        } else {
            System.out.println("Employee must not be null");
        }
    }

    public void removeEmployee(Employee employee) {
        if (employees.contains(employee)) {
            employees.remove(employee);
        } else {
            System.out.println("This employee is not in the list");
        }
    }

    public int getEmployeeCount() {
        return employees.size();
    }
}
