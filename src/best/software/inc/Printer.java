/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc;

import best.software.inc.people.Printable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author emman
 */
public class Printer {

    private List<Printable> jobs = new ArrayList<>();

    public void addJob(Printable printable) {
        jobs.add(printable);
    }

    public void removeJob(Printable printable) {
        jobs.remove(printable);
    }

    public void printAll() {
        for (Printable job : jobs) {
            job.print();
        }
    }
}
