/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.controllers;

import best.software.inc.models.EmployeesViewModelInterface;
import best.software.inc.ui.NewEmployeeView;

/**
 *
 * @author imjesr
 */
public class EmployeesController implements EmployeesViewControllerInterface {

    EmployeesViewModelInterface model;

    public EmployeesController(EmployeesViewModelInterface model) {
        this.model = model;
        model.initialize();
    }

    
    @Override
    public void loadEmployees() {
        
    }

    @Override
    public void viewEmployee() {

    }

    @Override
    public void newEmployee() {
        NewEmployeeControllerInterface controller = new NewEmployeeController();
        NewEmployeeView employeeView = new NewEmployeeView(model, controller);
        employeeView.setVisible(true);
    }

}
