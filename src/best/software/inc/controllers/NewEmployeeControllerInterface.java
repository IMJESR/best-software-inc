/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.controllers;

import best.software.inc.people.Employee;

/**
 *
 * @author imjesr
 */
public interface NewEmployeeControllerInterface {

    void saveEmployee(Employee employee);
}
