/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.controllers;

/**
 *
 * @author imjesr
 */
public interface EmployeesViewControllerInterface {

    void loadEmployees();

    void viewEmployee();

    public void newEmployee();
}
