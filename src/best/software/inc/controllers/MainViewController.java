/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.controllers;

import best.software.inc.models.EmployeesViewModel;
import best.software.inc.models.EmployeesViewModelInterface;
import best.software.inc.ui.EmployeesView;
import best.software.inc.ui.MainView;
import best.software.inc.models.MainViewModelInterface;
import best.software.inc.ui.SimpleMainView;

/**
 *
 * @author imjesr
 */
public class MainViewController implements MainViewControllerInterface {

    private MainViewModelInterface model;
    private MainView view;
    private SimpleMainView simpleView;

    public MainViewController(MainViewModelInterface model) {
        this.model = model;
        view = new MainView(this, model);
        view.setVisible(true);
        
        simpleView = new SimpleMainView(this, model);
        simpleView.setVisible(true);
    }

    @Override
    public void viewEmployees() {
        this.model.viewEmployees();
        
        
        EmployeesViewModelInterface model = new EmployeesViewModel();
        EmployeesViewControllerInterface controller = new EmployeesController(model);
        EmployeesView empView = new EmployeesView(model, controller);
        model.initialize();
        empView.setVisible(true);
        
    }

    @Override
    public void viewFinances() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void viewReports() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
