/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc;

import best.software.inc.people.SoftwareTeamLeader;
import java.util.List;

/**
 *
 * @author emman
 */
public class SoftwareProject extends Project {

    private List<Requirement> requirements;
    private SoftwareTeamLeader teamLeader;

    public List<Requirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(List<Requirement> requirements) {
        this.requirements = requirements;
    }

    public SoftwareTeamLeader getTeamLeader() {
        return teamLeader;
    }

    public void setTeamLeader(SoftwareTeamLeader teamLeader) {
        this.teamLeader = teamLeader;
    }

}
