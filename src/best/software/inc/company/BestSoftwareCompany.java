/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.company;

import best.software.inc.people.Printable;

/**
 *
 * @author emman
 */
public class BestSoftwareCompany extends Company implements Printable {

    @Override
    public void print() {
        System.out.println("Company Name: " + getName());
    }
}
