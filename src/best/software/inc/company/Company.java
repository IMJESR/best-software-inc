/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.company;

import best.software.inc.departments.Department;
import java.util.List;

/**
 *
 * @author emman
 */
public abstract class Company {

    private static int companyID;
    private String name;
    private List<Department> departments;

    public Company() {
        companyID++;
    }

    public static int getCompanyID() {
        return companyID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

}
