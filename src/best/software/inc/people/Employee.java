/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.people;

import best.software.inc.Position;
import java.util.Date;

/**
 *
 * @author emman
 */
public abstract class Employee extends Person {

    private static int employeeIDCount;
    private int employeeID;
    private float dailySalary;
    private Date startDate;
    private Date birthdate;
    private Employee boss;
    private Position position;

    public Employee(float dailySalary, String firstName, String lastName) {
        super(firstName, lastName);
        this.dailySalary = dailySalary;
        employeeID = employeeIDCount++;
    }

    public abstract float calculateMonthlySalary();

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public float getDailySalary() {
        return dailySalary;
    }

    public void setDailySalary(float dailySalary) {
        this.dailySalary = dailySalary;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Employee getBoss() {
        return boss;
    }

    public void setBoss(Employee boss) {
        this.boss = boss;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

}
