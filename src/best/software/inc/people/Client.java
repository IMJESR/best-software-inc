/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.people;

import best.software.inc.company.Company;

/**
 *
 * @author emman
 */
public class Client extends Person {

    private static int clientID;
    private Company company;

    public Client(String firstName, String lastName) {
        super(firstName, lastName);
        clientID++;
    }

}
