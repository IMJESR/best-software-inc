/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.people;

/**
 *
 * @author emman
 */
public abstract class Developer extends Employee {

    private String mainSkill;

    public Developer(float dailySalary, String firstName, String lastName) {
        super(dailySalary, firstName, lastName);
    }

    public String getMainSkill() {
        return mainSkill;
    }

    public void setMainSkill(String mainSkill) {
        this.mainSkill = mainSkill;
    }
}
