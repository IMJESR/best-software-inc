/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.people;

/**
 *
 * @author emman
 */
public class FrontEndDeveloper extends Developer {

    public FrontEndDeveloper(float dailySalary, String firstName, String lastName) {
        super(dailySalary, firstName, lastName);
    }

    @Override
    public float calculateMonthlySalary() {
        return 100 * 30;
    }

}
