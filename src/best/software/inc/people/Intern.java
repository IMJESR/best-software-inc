/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.people;

/**
 *
 * @author emman
 */
public class Intern extends Developer implements Printable {

    public Intern(float dailySalary, String firstName, String lastName) {
        super(dailySalary, firstName, lastName);
    }

    @Override
    public float calculateMonthlySalary() {
        return 0 * 30;
    }

    @Override
    public void print() {
        System.out.println("First Name: " + getFirstName());
        System.out.println("Last Name: " + getLastName());
    }
}
