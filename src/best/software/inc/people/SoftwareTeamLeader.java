/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.people;

import java.util.List;

/**
 *
 * @author emman
 */
public class SoftwareTeamLeader extends Employee {

    List<Developer> trainees;
    List<Developer> developers;

    public SoftwareTeamLeader(float dailySalary, String firstName, String lastName) {
        super(dailySalary, firstName, lastName);
    }

    @Override
    public float calculateMonthlySalary() {
        return 10 * 30;
    }
}
