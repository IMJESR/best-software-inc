/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc;

import best.software.inc.company.ISend;
import best.software.inc.people.Client;
import best.software.inc.people.Developer;
import java.util.Date;
import java.util.List;

/**
 *
 * @author emman
 */
public class Project implements ISend {

    private static int projectID;
    private String projectDescription;
    private Date startDate;
    private Date deadLine;
    private Client client;
    private float cost;
    private List<Developer> developers;

    public Project() {
        projectID++;
    }

    @Override
    public void send() {
        //  darle formato xml
        System.out.println("Enviando en formato XML");
    }

    public List<Developer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<Developer> developers) {
        this.developers = developers;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Date getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(Date deadLine) {
        this.deadLine = deadLine;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }
}
