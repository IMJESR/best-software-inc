/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.models;

import best.software.inc.people.Employee;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author imjesr
 */
public class EmployeesModelData extends AbstractTableModel {

    List<Employee> data = new ArrayList<>();
    String colNames[] = {"Employee ID", "First name", "Last name", "Salary"};
    Class<?> colClasses[] = {String.class, String.class, String.class, String.class};

    EmployeesModelData(List<Employee> employees) {
        data = employees;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return colNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return data.get(rowIndex).getEmployeeID();
        }
        if (columnIndex == 1) {
            return data.get(rowIndex).getFirstName();
        }
        if (columnIndex == 2) {
            return data.get(rowIndex).getLastName();
        }
        if (columnIndex == 3) {
            return data.get(rowIndex).getDailySalary();
        }
        return null;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return colNames[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return colClasses[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            data.get(rowIndex).setEmployeeID((Integer) aValue);
        }
        if (columnIndex == 1) {
            data.get(rowIndex).setFirstName((String) aValue);
        }
        if (columnIndex == 2) {
            data.get(rowIndex).setLastName((String) aValue);
        }
        if (columnIndex == 3) {
            data.get(rowIndex).setDailySalary((float) aValue);
        }
        fireTableCellUpdated(rowIndex, columnIndex);
    }
}
