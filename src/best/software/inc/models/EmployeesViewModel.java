/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.models;

import best.software.inc.people.BackEndDeveloper;
import best.software.inc.people.Employee;
import best.software.inc.ui.EmployeesViewObserver;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.TableModel;

/**
 *
 * @author imjesr
 */
public class EmployeesViewModel implements EmployeesViewModelInterface {

    private List<EmployeesViewObserver> observers;
    private List<Employee> employees;

    public EmployeesViewModel() {
        observers = new ArrayList<>();
    }

    @Override
    public void queryEmployees() {
        notifyObservers();
    }

    @Override
    public void viewSelectedEmployee() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initialize() {
        employees = getDummyData();
        notifyObservers();
    }

    @Override
    public void notifyObservers() {
        for (EmployeesViewObserver observer : observers) {
            observer.updateView();
        }
    }

    @Override
    public void registerObserver(EmployeesViewObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(EmployeesViewObserver observer) {
        observers.remove(observer);
    }

    @Override
    public TableModel getEmployeesTableModel() {
        return new EmployeesModelData(employees);
    }

    @Override
    public void addEmployee(Employee employee) {
        employees.add(employee);
        notifyObservers();
    }

    private List<Employee> getDummyData() {
        List<Employee> dummyEmployees = new ArrayList<>();
        dummyEmployees.add(new BackEndDeveloper(100, "Emmanuel", "Santana"));
        dummyEmployees.add(new BackEndDeveloper(100, "Luis", "Valero"));
        dummyEmployees.add(new BackEndDeveloper(100, "Erick", "Lara"));
        dummyEmployees.add(new BackEndDeveloper(100, "Francisco", "Valentin"));
        dummyEmployees.add(new BackEndDeveloper(100, "Sebastian", "Gaviria"));
        dummyEmployees.add(new BackEndDeveloper(100, "Amanda", "Torres"));
        return dummyEmployees;
    }

    
}
