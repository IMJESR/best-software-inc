/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.models;

import best.software.inc.people.Employee;
import best.software.inc.ui.EmployeesViewObserver;
import javax.swing.table.TableModel;

/**
 *
 * @author imjesr
 */
public interface EmployeesViewModelInterface {

    void queryEmployees();

    TableModel getEmployeesTableModel();

    void viewSelectedEmployee();

    void addEmployee(Employee employee);

    void initialize();

    void notifyObservers();

    void registerObserver(EmployeesViewObserver observer);

    void removeObserver(EmployeesViewObserver observer);
}
