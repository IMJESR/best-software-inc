/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc.models;

import best.software.inc.ModelObserver;

/**
 *
 * @author imjesr
 */
public interface MainViewModelInterface {

    void initialize();

    void notifyObservers();

    void registerObserver(ModelObserver observer);

    void removeObserver(ModelObserver observer);

    boolean isViewEmployeesEnabled();

    void viewEmployees();
}
