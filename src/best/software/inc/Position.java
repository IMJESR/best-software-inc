/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc;

/**
 *
 * @author emman
 */
public class Position {

    private static int positionID;
    private String name;

    public Position() {
        positionID++;
    }

    public static int getPositionID() {
        return positionID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
