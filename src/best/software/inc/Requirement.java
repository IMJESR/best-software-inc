/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package best.software.inc;

/**
 *
 * @author emman
 */
public class Requirement {

    private static int requirementID;
    private String description;
    private Priority priority = Priority.NOT_SET;

    public enum Priority {
        NOT_SET, LOW, MEDIUM, HIGH
    }

    public static int getRequirementID() {
        return requirementID;
    }

    public static void setRequirementID(int aRequirementID) {
        requirementID = aRequirementID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

}
